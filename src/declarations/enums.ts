enum BookingStatus {
  BOOKED = 'BOOKED',
  ACTIVE = 'ACTIVE',
  DONE = 'DONE',
  CANCELLED = 'CANCELLED',
}

export { BookingStatus };