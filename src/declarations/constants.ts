export const SMALL_INT_SIZE = 32_767;
export const INT_SIZE = 2_147_483_647;

/**
 * Search and infinite scroll related values
 */
export const MIN_SEARCH_TERM_LENGTH = 3;
export const CONTENT_SCROLL_BIAS = 192;

/**
 * Login related values
 */
export const MIN_LOGIN_LENGTH = 3;
export const MAX_LOGIN_LENGTH = 32;

/**
 * Amenities related values
 */
export const MIN_AMENITY_NAME_LENGTH = 3;
export const MAX_AMENITY_NAME_LENGTH = 64;
export const MIN_AMENITY_PROFIT = 0;
export const DEFAULT_AMENITY_PROFIT = 50;
export const AMENITIES_PAGE_SIZE = 18;

/**
 * Properties related values
 */
export const MIN_PROPERTY_NAME_LENGTH = 3;
export const MAX_PROPERTY_NAME_LENGTH = 64;
export const MIN_PROPERTY_CAPACITY = 1;
export const DEFAULT_CAPACITY_VALUE = 1;
export const PROPERTIES_PAGE_SIZE = 18;

/**
 * Rates related values
 */
export const MIN_RATE_NAME_LENGTH = 3;
export const MAX_RATE_NAME_LENGTH = 32;
export const MIN_RATE_PROFIT = 0;
export const DEFAULT_RATE_PROFIT = 500;
export const RATES_PAGE_SIZE = 18;

/**
 * Guests related values
 */
export const MIN_GUEST_PHONE_LENGTH = 11;
export const MAX_GUEST_PHONE_LENGTH = 11;
export const MIN_GUEST_NOTE_LENGTH = 3;
export const MAX_GUEST_NOTE_LENGTH = 64;
export const MIN_GUEST_NAME_LENGTH = 3;
export const MAX_GUEST_NAME_LENGTH = 48;
export const GUESTS_PAGE_SIZE = 16;

/**
 * Booking related values
 */
export const MAX_BOOKING_COMMENT_LENGTH = 255;
export const MIN_BOOKING_CORRECTION_REASON_LENGTH = 3;
export const MAX_BOOKING_CORRECTION_REASON_LENGTH = 64;
export const BOOKINGS_PAGE_SIZE = 16;