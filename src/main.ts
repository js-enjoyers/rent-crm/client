import { createApp } from 'vue';
import { createPinia } from 'pinia';

import { router } from './router';

import '@mdi/font/css/materialdesignicons.css';
import { createVuetify } from 'vuetify';
import { aliases, mdi } from 'vuetify/iconsets/mdi';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';

import { Chart, registerables } from 'chart.js';

import 'v-calendar/style.css';
import {
  setupCalendar,
  Calendar as PluginCalendar,
  DatePicker as PluginDatePicker,
} from 'v-calendar';

import './main.scss';
import './style.css';

import App from './App.vue';

const pinia = createPinia();

const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: { mdi },
  },
});

Chart.register(...registerables);

createApp(App)
  .provide('appName', 'Rent CRM')
  .use(router)
  .use(pinia)
  .use(vuetify)
  .use(setupCalendar, {})
  .component('PluginCalendar', PluginCalendar)
  .component('PluginDatePicker', PluginDatePicker)
  .mount('#app');
