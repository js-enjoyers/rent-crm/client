import { createRouter, createWebHistory } from 'vue-router';

import { routes } from './routes';
import { watchAuth, watchTitle } from './interceptors';

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach(watchTitle);
router.beforeResolve(watchAuth);

export { router };