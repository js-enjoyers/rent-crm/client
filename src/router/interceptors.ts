import { RouteLocationNormalized, NavigationGuardNext } from 'vue-router';
import { inject } from 'vue';

import { useAuth } from '@/composables';

const watchTitle = (route: RouteLocationNormalized) => {
  const appName = inject<string>('appName');
  const routeTitle = route.meta.title as string | undefined;

  document.title = routeTitle
    ? `${appName} | ${routeTitle}`
    : appName ?? '<unknown>';
};

const watchAuth = (route: RouteLocationNormalized, _: any, next: NavigationGuardNext) => {
  const auth = useAuth();

  const isLoginPage = route.path === '/login';
  const isSetupPage = route.path === '/setup';
  const isPublicPage = Boolean(route.meta.public);

  if ((isSetupPage || isLoginPage) && auth.isLoggedIn()) {
    return next('/dashboard');
  }

  if (!isPublicPage && !auth.isLoggedIn()) {
    return next('/login');
  }

  return next();
};


export { watchTitle, watchAuth };
