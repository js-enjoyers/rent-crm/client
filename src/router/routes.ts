import { RouteRecordRaw } from 'vue-router';

import Setup from '@/pages/setup/Setup.vue';
import LoginPage from '@/pages/login/LoginPage.vue';
import Dashboard from '@/pages/dashboard/Dashboard.vue';
import Bookings from '@/pages/bookings/Bookings.vue';
import BookingPage from '@/pages/bookings/BookingPage.vue';
import Guests from '@/pages/guests/Guests.vue';
import GuestPage from '@/pages/guests/GuestPage.vue';
import Properties from '@/pages/properties/Properties.vue';
import Rates from '@/pages/rates/Rates.vue';
import Amenities from '@/pages/amenities/Amenities.vue';
import Users from '@/pages/users/Users.vue';

export const routes: RouteRecordRaw[] = [
  { path: '/', redirect: '/login' },
  { path: '/setup', component: Setup, meta: { title: 'Начало работы', public: true } },
  { path: '/login', component: LoginPage, meta: { title: 'Авторизация', public: true } },
  { path: '/dashboard', component: Dashboard, meta: { title: 'Статистика', navigational: true, icon: 'mdi-chart-box-multiple-outline' } },
  { path: '/bookings', component: Bookings, meta: { title: 'Бронирования', navigational: true, icon: 'mdi-notebook-outline' } },
  { path: '/bookings/:id', component: BookingPage, meta: { title: 'Бронирования', navigational: false, icon: 'mdi-notebook-outline' } },
  { path: '/guests', component: Guests, meta: { title: 'Гости', navigational: true, icon: 'mdi-briefcase-account-outline' } },
  { path: '/guests/:id', component: GuestPage, meta: { title: 'Гости', navigational: false, icon: 'mdi-briefcase-account-outline' } },
  { path: '/properties', component: Properties, meta: { title: 'Объекты', navigational: true, icon: 'mdi-home-group' } },
  { path: '/rates', component: Rates, meta: { title: 'Тарифы', navigational: true, icon: 'mdi-cash-multiple' } },
  { path: '/amenities', component: Amenities, meta: { title: 'Услуги', navigational: true, icon: 'mdi-tag-multiple-outline' } },
  { path: '/users', component: Users, meta: { title: 'Пользователи', navigational: true, icon: 'mdi-account-circle-outline' } },
];