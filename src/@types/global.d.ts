type BookingStatus = import('@/declarations').BookingStatus;

type StoreFetchDataOptions = {
  term?: string;
  limit?: number;
  offset?: number;
  reset?: boolean;
}

type PageFetchDataOptions = {
  reset: boolean;
  useLoading: boolean;
};

interface IAuthorizedUser {
  id: number;
  login: string;
  accessToken: string | null;
}

interface IUser {
  id: number;
  login: string;
  createdAt: string;
}

interface IAmenity {
  id: number;
  name: string;
  profit: number | string;
  createdAt: string;
  deletedAt: string;
}

interface IBookingAmenity extends IAmenity {
  appliedProfit: string;
  quantity: number;
}

interface IProperty {
  id: number;
  name: string;
  color: string;
  capacity: number;
  createdAt: string;
  deletedAt: string;
}

interface IRate {
  id: number;
  name: string;
  profit: number | string;
  createdAt: string;
  deletedAt: string;
}

interface IGuestPhone {
  id: number;
  value: string;
  guestId: number;
  createdAt: string;
}

interface IGuestNote {
  id: number;
  text: string;
  guestId: number;
  createdAt: string;
}

interface IGuest {
  id: number;
  name: string;
  phones: IGuestPhone[];
  notes: IGuestNote[];
  bookings: Omit<IGeneralBooking, 'guest'>[];
  createdAt: string;
}

interface PhoneAvailabilityCheckResult {
  available: boolean;
  guest: IGuest | null;
}

interface IProfitCorrection {
  id: string;
  value: number | string;
  reason: string;
}

interface IBookingFilters {
  property: IProperty | null;
  dates: { start: Date | null, end: Date | null };
  guest: IGuest | null;
  status: BookingStatus | null;
}

interface INewBooking {
  property: IProperty | null;
  dates: { start: Date, end: Date } | null;
  rate: IRate | null;
  guests: {
    contact: IGuest | null;
    count: { adults: number; children: number; };
  };
  amenities: IBookingAmenity[];
  corrections: IProfitCorrection[];
  comment: string;
}

interface IGeneralBooking {
  id: number;
  status: BookingStatus;
  property: IProperty;
  guest: Omit<IGuest, 'notes' | 'phones'>,
  startDate: string;
  endDate: string;
  createdAt: string;
  updatedAt: string;
}

interface IVerboseBooking {
  id: number;
  status: BookingStatus;
  totalProfit: string;
  appliedRateProfit: string;
  startDate: string;
  endDate: string;
  adults: number;
  children: number;
  comment: string | null;
  property: IProperty;
  rate: IRate;
  guest: IGuest;
  corrections: IProfitCorrection[];
  amenities: IBookingAmenity[];
  createdAt: string;
  updatedAt: string;
}