import { AxiosRequestConfig, AxiosResponse as BaseAxiosResponse } from 'axios';
import { axiosInstance } from './axios.ts';

interface ServerErrorPayload {
  message: string;
  error: string;
  statusCode: number;
}

interface AxiosResponse<T = any> extends BaseAxiosResponse {
  data: {
    values: T | null;
    error: ServerErrorPayload | null;
  };
}

/**
 * It's just proxy of original axios instance to
 * support custom response format. Tried to do this
 * using `declare` TS feature, but it didn't work, so
 * here is pretty straightforward solution
 */
export class HttpClient {
  static get<T = any, D = any>(url: string, config?: AxiosRequestConfig<D>) {
    return axiosInstance.get<T>(url, config) as Promise<AxiosResponse<T>>;
  }

  static post<T = any, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>) {
    return axiosInstance.post<T>(url, data, config) as Promise<AxiosResponse<T>>;
  }

  static put<T = any, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>) {
    return axiosInstance.put<T>(url, data, config) as Promise<AxiosResponse<T>>;
  }

  static patch<T = any, D = any>(url: string, data?: D, config?: AxiosRequestConfig<D>) {
    return axiosInstance.patch<T>(url, data, config) as Promise<AxiosResponse<T>>;
  }

  static delete<T = any, D = any>(url: string, config?: AxiosRequestConfig<D>) {
    return axiosInstance.delete<T>(url, config) as Promise<AxiosResponse<T>>;
  }
}