import axios, { AxiosResponse } from 'axios';

import { useAuth } from '@/composables';
import { sleep } from '@/utils';

const USE_THROTTLING = false;
const THROTTLING_TIME = 1000;

const API_BASE_URL = import.meta.env.VITE_API_BASE_URL;
const axiosInstance = axios.create();

const throttleRequest = async () => {
  if (!USE_THROTTLING) return;
  await sleep(THROTTLING_TIME);
};

const getQueryToken = () => {
  const { currentUser } = useAuth();

  return currentUser.value
    ? `Bearer ${currentUser.value.accessToken}`
    : '<unauthorized>';
};

const buildResponse = (response: AxiosResponse, data: any) => {
  response.data = data;
  return response;
};

axiosInstance.interceptors.request.use(async config => {
  if (!API_BASE_URL) {
    throw new Error('VITE_API_BASE_URL is not defined');
  }

  await throttleRequest();

  config.headers.Authorization = getQueryToken();
  config.baseURL = API_BASE_URL;

  return config;
});

axiosInstance.interceptors.response.use(
  response => {
    return buildResponse(response, {
      values: response.data, error: null,
    });
  },
  /**
   * If axios error has occurred, format
   * response to a common structure and
   * just reject promise to pass handling
   * to global error boundary
   */
  error => {
    if (!error.response) {
      throw new Error(`Не удалось выполнить запрос: ${error.message}`);
    }

    Promise.reject(error);

    return buildResponse(error.response, {
      error: error.response?.data ?? { message: 'Неизвестная ошибка' },
      values: null,
    });
  },
);

export { axiosInstance };