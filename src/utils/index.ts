import { useDateTime } from '@/composables';

const dt = useDateTime();

export const isDefined = (value: any) => value !== undefined;

export const isArchived = (data: any) => Boolean(data?.deletedAt);

export const sleep = (delay: number) => {
  return new Promise(resolve => {
    setTimeout(resolve, delay);
  });
};

export const debounce = <T extends (...args: any[]) => any>(delay: number, fn: T): (...args: Parameters<T>) => void => {
  let timeoutID: number | undefined;

  return (...args: Parameters<T>) => {
    if (timeoutID) clearTimeout(timeoutID);

    // @ts-expect-error setTimeout returns number in browser environment
    timeoutID = setTimeout(() => {
      fn(...args);
    }, delay);
  };
};

export const getFormattedPhoneNumber = (phone: string) => {
  const countryCode = phone.at(0)!;
  const operatorCode = phone.slice(1, 4);

  const numberParts = (
    [phone.slice(4, 7), phone.slice(7, 9), phone.slice(9)].join('-')
  );

  return `+${countryCode} (${operatorCode}) ${numberParts}`;
};

export const getNightsCount = (start: Date | string, end: Date | string) => {
  const result = dt.wrap(end)
    .diff(dt.wrap(start), 'days')
    .days;

  return Math.ceil(result);
};

export const getCapacityLabel = (capacity: number) => {
  const lastDigit = capacity % 10;
  const lastTwoDigits = capacity % 100;

  if (lastTwoDigits >= 11 && lastTwoDigits <= 19) return 'мест';
  if (lastDigit === 1) return 'место';
  if (lastDigit >= 2 && lastDigit <= 4) return 'места';
  return 'мест';
};

export const getNightsLabel = (days: number) => {
  const lastDigit = days % 10;
  const lastTwoDigits = days % 100;

  if (lastTwoDigits >= 11 && lastTwoDigits <= 14) return 'ночей';
  if (lastDigit === 1) return 'ночь';
  if ([2, 3, 4].includes(lastDigit)) return 'ночи';
  return 'ночей';
};

export const getGuestsLabel = (guests: number) => {
  const lastDigit = guests % 10;
  const lastTwoDigits = guests % 100;

  if (lastTwoDigits >= 11 && lastTwoDigits <= 19) return 'гостей';
  if (lastDigit === 1) return 'гость';
  if (lastDigit >= 2 && lastDigit <= 4) return 'гостя';
  return 'гостей';
};

export const getTimesLabel = (times: number) => {
  const lastDigit = times % 10;
  const lastTwoDigits = times % 100;

  if (lastTwoDigits >= 11 && lastTwoDigits <= 19) return 'раз';
  if (lastDigit === 1) return 'раз';
  if (lastDigit >= 2 && lastDigit <= 4) return 'раза';
  return 'раз';
};