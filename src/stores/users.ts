import { ref } from 'vue';
import { defineStore } from 'pinia';

import { HttpClient } from '@/http';

type CreateUserPayload = Pick<IUser, 'login'> & { pin: string };

export const useUsersStore = defineStore('users', () => {
  const users = ref<IUser[]>([]);

  const fetchData = async () => {
    users.value = [];

    const response = (
      await HttpClient.get<IUser[]>('/v1/users')
    );

    if (response.data.values) {
      users.value = response.data.values;
    }

    return response;
  };

  const createUser = async (user: CreateUserPayload) => {
    const response = (
      await HttpClient.post<IUser>('/v1/users', user)
    );

    if (response.data.values) {
      users.value.unshift(response.data.values);
    }

    return response;
  };

  const deleteUser = async (targetUser: IUser) => {
    const id = targetUser.id;

    const { data: { error } } = (
      await HttpClient.delete(`/v1/users/${id}`)
    );

    if (error) return;

    const targetIndex = users.value.findIndex(user => {
      return user.id === id;
    });

    if (targetIndex < 0) return;
    users.value.splice(targetIndex, 1);
  };

  return { users, fetchData, createUser, deleteUser };
});