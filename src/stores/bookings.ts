import { defineStore } from 'pinia';
import { ref } from 'vue';

import { HttpClient } from '@/http';
import { useDateTime } from '@/composables';

type BookingsFetchOptions = (
  StoreFetchDataOptions & Partial<IBookingFilters>
);

const dt = useDateTime();

const getRequestParams = (options: Omit<BookingsFetchOptions, 'reset' | 'term'>) => {
  const start = options.dates?.start &&
    encodeURIComponent(options.dates.start.toISOString());

  const end = options.dates?.end &&
    encodeURIComponent(options.dates.end.toISOString());

  return {
    ...(options.limit && { limit: options.limit }),
    ...(options.offset && { offset: options.offset }),
    ...(options.property && { propertyId: options.property.id }),
    ...(options.guest && { guestId: options.guest.id }),
    ...(options.status && { status: options.status }),
    ...(start && { start }),
    ...(end && { end }),
  };
};

export const useBookingsStore = defineStore('bookings', () => {
  const bookings = ref<IGeneralBooking[]>([]);

  const fetchData = async (options: BookingsFetchOptions) => {
    const { reset } = options ?? {};

    if (reset) {
      bookings.value = [];
    }

    const response = (
      await HttpClient.get<IGeneralBooking[]>('/v1/bookings', {
        params: getRequestParams(options),
      })
    );

    if (response.data.values) {
      bookings.value = bookings.value.concat(response.data.values);
    }

    return response;
  };

  const createBooking = async (booking: INewBooking) => {
    const amenities = booking.amenities.map(amenity => {
      return { amenityId: amenity.id, quantity: amenity.quantity };
    });

    const corrections = booking.corrections.map(({ value, reason }) => {
      return { value, reason };
    });

    const response = await HttpClient.post('/v1/bookings', {
      propertyId: booking.property!.id,
      startDate: dt.wrap(booking.dates!.start).toISO(),
      endDate: dt.wrap(booking.dates!.end).toISO(),
      rateId: booking.rate!.id,
      guestId: booking.guests.contact!.id,
      adults: booking.guests.count.adults,
      children: booking.guests.count.children,
      ...(booking.comment && { comment: booking.comment }),
      amenities,
      corrections,
    });

    if (!response.data.values) {
      return response;
    }

    const newBookingStartDate = booking.dates!.start;

    const previousBookingIndex = bookings.value.findIndex(booking => {
      return Number(new Date(booking.startDate)) < Number(new Date(newBookingStartDate));
    });

    if (previousBookingIndex === -1) {
      bookings.value.unshift(response.data.values);
      return response;
    }

    bookings.value.splice(previousBookingIndex, 0, response.data.values);

    return response;
  };

  return {
    bookings,
    fetchData,
    createBooking,
  };
});