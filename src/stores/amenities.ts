import { defineStore } from 'pinia';
import { ref, computed } from 'vue';

import { HttpClient } from '@/http';

export const useAmenitiesStore = defineStore('amenities', () => {
  const showArchived = ref(false);
  const dataset = ref<IAmenity[]>([]);

  const amenities = computed(() => {
    return dataset.value.filter(amenity => {
      const isArchived = Boolean(amenity.deletedAt);

      if (!isArchived) return true;
      return showArchived.value;
    });
  });

  const fetchData = async (options: StoreFetchDataOptions) => {
    const { reset, ...requestParams } = options ?? {};

    if (reset) {
      dataset.value = [];
    }

    const response = (
      await HttpClient.get<IAmenity[]>('/v1/amenities', {
        params: { ...requestParams, archive: true },
      })
    );

    if (response.data.values) {
      dataset.value = dataset.value.concat(response.data.values);
    }

    return response;
  };

  const createAmenity = async (data: Pick<IAmenity, 'name' | 'profit'>) => {
    const response = (
      await HttpClient.post<IAmenity>('/v1/amenities', data)
    );

    if (response.data.values) {
      dataset.value.unshift(response.data.values);
    }

    return response;
  };

  const editAmenity = async (
    id: IAmenity['id'],
    data: Pick<IAmenity, 'name' | 'profit'>,
  ) => {
    const response = (
      await HttpClient.put(`/v1/amenities/${id}`, data)
    );

    if (response.data.error) return response;

    const targetAmenity = (
      dataset.value.find(amenity => amenity.id === id)
    );

    if (targetAmenity && response.data.values) {
      Object.assign(targetAmenity, response.data.values);
    }

    return response;
  };

  const archiveAmenity = async (id: IAmenity['id']) => {
    const response = (
      await HttpClient.delete<IAmenity>(`/v1/amenities/${id}`)
    );

    if (response.data.error) return response;

    const targetAmenity = (
      dataset.value.find(amenity => amenity.id === id)
    );

    if (targetAmenity && response.data.values) {
      Object.assign(targetAmenity, {
        deletedAt: response.data.values.deletedAt,
      });
    }

    return response;
  };

  const restoreAmenity = async (id: IAmenity['id']) => {
    const response = (
      await HttpClient.put<IAmenity>(`/v1/amenities/${id}/restore`)
    );

    if (response.data.error) return response;

    const targetAmenity = (
      dataset.value.find(amenity => amenity.id === id)
    );

    if (targetAmenity && response.data.values) {
      Object.assign(targetAmenity, {
        deletedAt: response.data.values.deletedAt,
      });
    }

    return response;
  };

  return {
    showArchived,
    amenities,
    fetchData,
    createAmenity,
    editAmenity,
    archiveAmenity,
    restoreAmenity,
  };
});