import { defineStore } from 'pinia';
import { ref } from 'vue';

import { HttpClient } from '@/http';

export const useGuestsStore = defineStore('guests', () => {
  const guests = ref<IGuest[]>([]);

  const fetchData = async (options: StoreFetchDataOptions) => {
    const { reset, ...requestParams } = options ?? {};

    if (reset) {
      guests.value = [];
    }

    const response = (
      await HttpClient.get<IGuest[]>('/v1/guests', {
        params: requestParams,
      })
    );

    if (response.data.values) {
      guests.value = guests.value.concat(response.data.values);
    }

    return response;
  };

  const createGuest = async (data: Pick<IGuest, 'name'>) => {
    const response = (
      await HttpClient.post<IGuest>('/v1/guests', data)
    );

    if (response.data.values) {
      guests.value.unshift(response.data.values);
    }

    return response;
  };

  const editGuest = async (id: IGuest['id'], data: Pick<IGuest, 'name'>) => {
    const targetGuest = (
      guests.value.find(guest => guest.id === id)
    );

    if (!targetGuest) {
      return;
    }

    const response = (
      await HttpClient.put(`/v1/guests/${id}`, data)
    );

    if (response.data.values) {
      Object.assign(targetGuest, response.data.values);
    }

    return response;
  };

  const createGuestPhone = async (
    data: Pick<IGuestPhone, 'value'> & { guestId: IGuest['id'], force: boolean },
  ) => {
    const response = (
      await HttpClient.post<IGuestPhone>('/v1/guests/phones', data)
    );

    if (response.data.error) {
      return response;
    }

    const targetGuest = (
      guests.value.find(guest => guest.id === data.guestId)
    );

    const oldPhoneOwner = (
      guests.value.find(guest => {
        return guest.phones.find(phone => {
          return phone.value === data.value;
        });
      })
    );

    if (oldPhoneOwner) {
      const targetPhoneIndex = (
        oldPhoneOwner.phones.findIndex(phone => {
          return phone.value === data.value;
        })
      );

      oldPhoneOwner.phones.splice(targetPhoneIndex, 1);
    }

    if (response.data.values && targetGuest) {
      targetGuest.phones.unshift(response.data.values);
    }

    return response;
  };

  const deleteGuestPhone = async (id: IGuestPhone['id'], guestId: IGuest['id']) => {
    const response = (
      await HttpClient.delete<IGuestPhone>(`/v1/guests/phones/${id}`)
    );

    if (response.data.error) return response;

    const targetGuest = (
      guests.value.find(guest => guest.id === guestId)!
    );

    const targetPhoneIndex = (
      targetGuest.phones.findIndex((phone: IGuestPhone) => {
        return phone.id === id;
      })
    );

    targetGuest.phones.splice(targetPhoneIndex, 1);

    return response;
  };

  const createGuestNote = async (
    data: Pick<IGuestNote, 'text'> & { guestId: IGuest['id'] },
  ) => {
    const response = (
      await HttpClient.post<IGuestNote>('/v1/guests/notes', data)
    );

    const targetGuest = (
      guests.value.find(guest => guest.id === data.guestId)
    );

    if (response.data.values && targetGuest) {
      targetGuest.notes.unshift(response.data.values);
    }

    return response;
  };

  const deleteGuestNote = async (id: IGuestNote['id'], guestId: IGuest['id']) => {
    const response = (
      await HttpClient.delete<IGuestNote>(`/v1/guests/notes/${id}`)
    );

    if (response.data.error) return response;

    const targetGuest = (
      guests.value.find(guest => guest.id === guestId)!
    );

    const targetNoteIndex = (
      targetGuest.notes.findIndex((note: IGuestNote) => {
        return note.id === id;
      })
    );

    targetGuest.notes.splice(targetNoteIndex, 1);

    return response;
  };

  return {
    guests,
    fetchData,
    createGuest,
    editGuest,
    createGuestPhone,
    deleteGuestPhone,
    createGuestNote,
    deleteGuestNote
  };
});