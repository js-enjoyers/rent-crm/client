import { defineStore } from 'pinia';
import { ref, computed } from 'vue';

import { HttpClient } from '@/http';

export const useRatesStore = defineStore('rates', () => {
  const showArchived = ref(false);
  const dataset = ref<IRate[]>([]);

  const rates = computed(() => {
    return dataset.value.filter(rate => {
      const isArchived = Boolean(rate.deletedAt);

      if (!isArchived) return true;
      return showArchived.value;
    });
  });

  const fetchData = async (options: StoreFetchDataOptions) => {
    const { reset, ...requestParams } = options ?? {};

    if (reset) {
      dataset.value = [];
    }

    const response = (
      await HttpClient.get<IRate[]>('/v1/rates', {
        params: { ...requestParams, archive: true },
      })
    );

    if (response.data.values) {
      dataset.value = dataset.value.concat(response.data.values);
    }

    return response;
  };

  const createRate = async (data: Pick<IRate, 'name' | 'profit'>) => {
    const response = (
      await HttpClient.post<IRate>('/v1/rates', data)
    );

    if (response.data.values) {
      dataset.value.unshift(response.data.values);
    }

    return response;
  };

  const editRate = async (
    id: IRate['id'],
    data: Pick<IRate, 'name' | 'profit'>,
  ) => {
    const response = (
      await HttpClient.put(`/v1/rates/${id}`, data)
    );

    if (response.data.error) return response;

    const targetRate = (
      dataset.value.find(rate => rate.id === id)
    );

    if (targetRate && response.data.values) {
      Object.assign(targetRate, response.data.values);
    }

    return response;
  };

  const archiveRate = async (id: IRate['id']) => {
    const response = (
      await HttpClient.delete<IRate>(`/v1/rates/${id}`)
    );

    if (response.data.error) return response;

    const targetRate = (
      dataset.value.find(rate => rate.id === id)
    );

    if (targetRate && response.data.values) {
      Object.assign(targetRate, {
        deletedAt: response.data.values.deletedAt,
      });
    }

    return response;
  };

  const restoreRate = async (id: IRate['id']) => {
    const response = (
      await HttpClient.put<IRate>(`/v1/rates/${id}/restore`)
    );

    if (response.data.error) return response;

    const targetRate = (
      dataset.value.find(rate => rate.id === id)
    );

    if (targetRate && response.data.values) {
      Object.assign(targetRate, {
        deletedAt: response.data.values.deletedAt,
      });
    }

    return response;
  };

  return {
    showArchived,
    rates,
    fetchData,
    createRate,
    editRate,
    archiveRate,
    restoreRate,
  };
});