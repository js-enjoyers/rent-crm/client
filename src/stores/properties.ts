import { defineStore } from 'pinia';
import { ref, computed } from 'vue';

import { HttpClient } from '@/http';

export const usePropertiesStore = defineStore('properties', () => {
  const showArchived = ref(false);
  const dataset = ref<IProperty[]>([]);

  const properties = computed(() => {
    return dataset.value.filter(property => {
      const isArchived = Boolean(property.deletedAt);

      if (!isArchived) return true;
      return showArchived.value;
    });
  });

  const fetchData = async (options: StoreFetchDataOptions) => {
    const { reset, ...requestParams } = options ?? {};

    if (reset) {
      dataset.value = [];
    }

    const response = (
      await HttpClient.get<IProperty[]>('/v1/properties', {
        params: { ...requestParams, archive: true },
      })
    );

    if (response.data.values) {
      dataset.value = dataset.value.concat(response.data.values);
    }

    return response;
  };

  const createProperty = async (data: Pick<IProperty, 'name' | 'capacity' | 'color'>) => {
    const response = (
      await HttpClient.post<IProperty>('/v1/properties', data)
    );

    if (response.data.values) {
      dataset.value.unshift(response.data.values);
    }

    return response;
  };

  const editProperty = async (
    id: IProperty['id'],
    data: Pick<IProperty, 'name' | 'capacity' | 'color'>
  ) => {
    const response = (
      await HttpClient.put(`/v1/properties/${id}`, data)
    );

    if (response.data.error) return response;

    const targetProperty = (
      dataset.value.find(property => property.id === id)
    );

    if (targetProperty && response.data.values) {
      Object.assign(targetProperty, response.data.values);
    }

    return response;
  };

  const archiveProperty = async (id: IProperty['id']) => {
    const response = (
      await HttpClient.delete<IProperty>(`/v1/properties/${id}`)
    );

    if (response.data.error) return response;

    const targetProperty = (
      dataset.value.find(property => property.id === id)
    );

    if (targetProperty && response.data.values) {
      Object.assign(targetProperty, {
        deletedAt: response.data.values.deletedAt,
      });
    }

    return response;
  };

  const restoreProperty = async (id: IProperty['id']) => {
    const response = (
      await HttpClient.put<IAmenity>(`/v1/properties/${id}/restore`)
    );

    if (response.data.error) return response;

    const targetProperty = (
      dataset.value.find(property => property.id === id)
    );

    if (targetProperty && response.data.values) {
      Object.assign(targetProperty, {
        deletedAt: response.data.values.deletedAt,
      });
    }

    return response;
  };

  return {
    showArchived,
    properties,
    fetchData,
    createProperty,
    editProperty,
    archiveProperty,
    restoreProperty,
  };
});