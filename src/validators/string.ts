import { isDefined } from '@/utils';

type StringValidatorOptions = {
  required?: boolean;
  length?: {
    min?: number;
    max?: number;
  }
}

const REQUIRED_ERROR_MESSAGE = 'Это обязательное поле';
const MIN_LENGTH_ERROR_MESSAGE = 'Минимум символов: $1';
const MAX_LENGTH_ERROR_MESSAGE = 'Максимум символов: $1';

const createStringValidator = (options: StringValidatorOptions) => {
  const validateLength = (value: string) => {
    const minLength = options.length?.min;
    const maxLength = options.length?.max;

    if (isDefined(minLength) && (value.length < minLength!)) {
      return MIN_LENGTH_ERROR_MESSAGE.replace('$1', String(minLength));
    }

    if (isDefined(maxLength) && (value.length > maxLength!)) {
      return MAX_LENGTH_ERROR_MESSAGE.replace('$1', String(maxLength));
    }

    return true;
  };

  return (value: string) => {
    const trimmedValue = value?.trim();

    if (!trimmedValue) {
      return options.required ? REQUIRED_ERROR_MESSAGE : true;
    }

    return validateLength(trimmedValue);
  };
};

export { createStringValidator };