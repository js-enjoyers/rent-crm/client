import { DateTime } from 'luxon';

type DateInput = string | Date;

const DEFAULT_LOCALE = 'ru';

const useDateTime = () => {
  /**
   * @description Get displayable date time string
   *
   * @param date target date
   * @param format target format string
   *
   * @example
   * dt.display('2014-08-06T13:07:04.054', 'dd MMMM yyyy')
   */
  const display = (date: DateInput, format?: string) => {
    const dt = DateTime.fromISO(
      new Date(date).toISOString()
    ).toLocal();

    if (format) {
      return dt.setLocale(DEFAULT_LOCALE).toFormat(format);
    }

    return dt.toLocaleString(
      DateTime.DATE_MED,
      { locale: DEFAULT_LOCALE },
    );
  };

  const wrap = (date: DateInput) => {
    return DateTime.fromISO(new Date(date).toISOString()).toLocal();
  };

  return { display, wrap, origin: DateTime };
};

export { useDateTime };