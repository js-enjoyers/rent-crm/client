import { ref } from 'vue';

const LOCAL_STORAGE_KEY = 'auth.savedUser';

const useAuth = () => {
  const currentUser = ref<IAuthorizedUser | null>(null);
  const jsonData = localStorage.getItem(LOCAL_STORAGE_KEY);

  if (jsonData) {
    try {
      currentUser.value = JSON.parse(jsonData) as IAuthorizedUser;
    } catch {
      console.warn('Couldn\'t get authorized user from device data');
    }
  }

  const login = (user: IAuthorizedUser) => {
    currentUser.value = user;
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(user));
  };

  const saveAccount = (user: Omit<IAuthorizedUser, 'accessToken'>) => {
    login({ ...user, accessToken: null });
  };

  const removeAccount = () => {
    currentUser.value = null;
    localStorage.removeItem(LOCAL_STORAGE_KEY);
  };

  const logout = () => {
    if (!currentUser.value) return;
    login({ ...currentUser.value, accessToken: null });
  };

  const isLoggedIn = () => {
    return Boolean(currentUser.value?.accessToken);
  };

  const getLogin = () => {
    return currentUser.value?.login ?? null;
  };

  return {
    currentUser,
    login,
    saveAccount,
    removeAccount,
    logout,
    isLoggedIn,
    getLogin,
  };
};

export { useAuth };