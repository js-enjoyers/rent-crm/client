import { ref } from 'vue';

type UsePaginationOptions = {
  size: number;
}

const usePagination = ({ size }: UsePaginationOptions) => {
  const currentPage = ref(1);
  const offset = ref(0);

  const calculateOffset = () => {
    offset.value = (currentPage.value - 1) * size;
  };

  const resetPage = () => {
    currentPage.value = 1;
    calculateOffset();
  };

  const nextPage = () => {
    currentPage.value = currentPage.value + 1;
    calculateOffset();
  };

  return { offset, currentPage, resetPage, nextPage };
};

export { usePagination };