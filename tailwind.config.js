/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    ({ addBase, theme }) => {
      addBase({
        ':root': {
          '--tw-color-teal-600': theme('colors.teal.600'),
          '--tw-color-gray-400': theme('colors.gray.400'),
        },
      });
    }
  ],
};

